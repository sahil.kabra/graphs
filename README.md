Graph Sample
==========

This demos the below operations using graphs
- Djikstras shortest path algorithm
- Eccentricity of a graph
- Radius of a graph
- Diameter of a graph

Language
==========
- Java 8
- Gradle

Project Structure
==========
- `GraphUtils` contains the entry points for all the graph functions
- `Djikstras` contains an implementation of the shortest path algorithm
- `Graph` is the main class that holds the entire graph.
- `Node` represents a node of the graph.
- `Edge` represents an edge from :code:`fromNode` to :code:`toNode` with weight :code:`weight`

Instructions
==========
- Run `./gradlew build` to create the uber jar.
- Run the command `java -jar build/libs/graphs-all.jar <<numNodes>> <<sparseness>>`

This will randomly generate a graph with `numNodes` and `sparseness`.

It will then print:
- shortest path between two randomly selected nodes.
- eccentricity of a random node
- diameter of the graph
- radius of the graph

Please ensure that the `sparseness` is between `N - 1` and `N(N-1)/2`. There is no check for this in the application.
