package demo.graph;

import demo.graph.model.Graph;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static demo.graph.GraphUtils.*;
import static org.assertj.core.api.Assertions.assertThat;

class GraphUtilsTest {

    @Nested
    class EccentricityTest {
        @Test
        public void shouldCalculateEccentricityForSample() {
            Graph g = new Graph(Arrays.asList("A", "B", "C", "D", "E", "F"));

            g.addEdge("A", "B", 10);
            g.addEdge("A", "C", 15);
            g.addEdge("B", "D", 12);
            g.addEdge("B", "F", 15);
            g.addEdge("C", "E", 10);
            g.addEdge("D", "F", 1);
            g.addEdge("D", "E", 2);
            g.addEdge("F", "E", 5);

            int eccentricity = eccentricity(g, "A");

            assertThat(eccentricity).isEqualTo(24);
        }

        @Test
        public void shouldReturnCorrectEccentricityForGraphWithOneShortestPath() {
            Graph g = new Graph(Arrays.asList("A", "B", "C", "D", "E", "F", "G"));

            g.addEdge("A", "B", 1);
            g.addEdge("B", "C", 1);
            g.addEdge("B", "D", 1);
            g.addEdge("B", "F", 1);
            g.addEdge("D", "E", 1);
            g.addEdge("F", "G", 1);
            g.addEdge("E", "D", 1);
            g.addEdge("D", "B", 1);

            assertThat(eccentricity(g, "A")).isEqualTo(3);
            assertThat(eccentricity(g, "B")).isEqualTo(2);
            assertThat(eccentricity(g, "E")).isEqualTo(4);
            assertThat(eccentricity(g, "C")).isEqualTo(0);
        }
    }

    @Nested
    class DiameterTest {

        @Test
        public void shouldWorkForSingleNode() {
            Graph g = new Graph(Collections.singletonList("A"));

            assertThat(diameter(g)).isEqualTo(0);
        }

        @Test
        public void shouldWorkForSingleEdge() {
            Graph g = new Graph(Arrays.asList("A", "B"));

            g.addEdge("A", "B", 1);

            assertThat(diameter(g)).isEqualTo(1);
        }

        @Test
        public void shouldReturnCorrectDiameterForGraphWithOneShortestPath() {
            Graph g = new Graph(Arrays.asList("A", "B", "C", "D", "E", "F", "G"));

            g.addEdge("A", "B", 1);
            g.addEdge("B", "A", 1);

            g.addEdge("B", "C", 1);
            g.addEdge("C", "B", 1);

            g.addEdge("B", "D", 1);
            g.addEdge("D", "B", 1);

            g.addEdge("B", "F", 1);
            g.addEdge("F", "B", 1);

            g.addEdge("D", "E", 1);
            g.addEdge("E", "D", 1);

            g.addEdge("F", "G", 1);
            g.addEdge("G", "F", 1);

            assertThat(diameter(g)).isEqualTo(4);
        }
    }

    @Nested
    class RadiusTest {

        @Test
        public void shouldWorkForSingleNode() {
            Graph g = new Graph(Collections.singletonList("A"));

            assertThat(radius(g)).isEqualTo(0);
        }

        @Test
        public void shouldWorkForSingleEdge() {
            Graph g = new Graph(Arrays.asList("A", "B"));

            g.addEdge("A", "B", 1);

            assertThat(radius(g)).isEqualTo(0);
        }

        @Test
        public void shouldReturnCorrectValueGraphWithOneShortestPath() {
            Graph g = new Graph(Arrays.asList("A", "B", "C", "D", "E", "F", "G"));

            g.addEdge("A", "B", 1);
            g.addEdge("B", "A", 1);

            g.addEdge("B", "C", 1);
            g.addEdge("C", "B", 1);

            g.addEdge("B", "D", 1);
            g.addEdge("D", "B", 1);

            g.addEdge("B", "F", 1);
            g.addEdge("F", "B", 1);

            g.addEdge("D", "E", 1);
            g.addEdge("E", "D", 1);

            g.addEdge("F", "G", 1);
            g.addEdge("G", "F", 1);

            assertThat(radius(g)).isEqualTo(2);
        }
    }
}