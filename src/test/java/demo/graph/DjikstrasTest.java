package demo.graph;

import demo.graph.model.Graph;
import demo.graph.model.Node;
import demo.graph.model.Path;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

class DjikstrasTest {

    @Test
    public void shouldFindPathForTwoNodes() {
        Graph g = new Graph(Arrays.asList("0", "1"));

        g.addEdge("0", "1", 5);

        Path path = Djikstras.findShortestPath(g, "0", "1");

        assertThat(path.getNodes()).hasSize(2);
        assertThat(path.getNodes()).extracting(Node::getName).contains("0", "1");
        assertThat(path.getTotalWeight()).isEqualTo(5);
    }

    @Test
    public void shouldFindPathForSampleGraph() {
        Graph g = new Graph(Arrays.asList("A", "B", "C", "D", "E", "F"));

        g.addEdge("A", "B", 10);
        g.addEdge("A", "C", 15);
        g.addEdge("B", "D", 12);
        g.addEdge("B", "F", 15);
        g.addEdge("C", "E", 10);
        g.addEdge("D", "F", 1);
        g.addEdge("D", "E", 2);
        g.addEdge("F", "E", 5);

        Path path = Djikstras.findShortestPath(g, "A", "E");

        assertThat(path.getNodes()).hasSize(4);
        assertThat(path.getNodes()).extracting(Node::getName).contains("A", "B", "D", "E");
        assertThat(path.getTotalWeight()).isEqualTo(24);
    }

    @Test
    public void shouldReturnEmptyWhenNoPath() {
        Graph g = new Graph(Arrays.asList("0", "1"));

        g.addEdge("0", "1", 5);

        Path path = Djikstras.findShortestPath(g, "1", "0");

        assertThat(path.getNodes()).isEmpty();
        assertThat(path.getTotalWeight()).isEqualTo(0);
    }
}