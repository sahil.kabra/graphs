package demo.graph.model;

import demo.graph.model.Edge;
import demo.graph.model.Graph;
import demo.graph.model.Node;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class GraphTest {

    @Test
    public void shouldCreateSimpleGraphWithTwoNodes() {
        Graph g = new Graph(2, 1);

        Collection<Node> nodes = g.getNodes();
        Collection<Edge> edges = nodes.stream().flatMap(n -> n.getEdges().stream()).collect(Collectors.toList());

        assertThat(nodes).hasSize(2);
        assertThat(edges).hasSize(1);
    }

    @Test
    public void shouldCreateSimpleGraphWithThreeNodes() {
        Graph g = new Graph(3, 2);

        Collection<Node> nodes = g.getNodes();
        Collection<Edge> edges = nodes.stream().flatMap(n -> n.getEdges().stream()).collect(Collectors.toList());

        assertThat(nodes).hasSize(3);
        assertThat(edges).hasSize(2);
    }

    @Test
    public void shouldCreateFullyConnectedGraphWithThreeNodes() {
        Graph g = new Graph(3, 3);

        Collection<Node> nodes = g.getNodes();
        Collection<Edge> edges = nodes.stream().flatMap(n -> n.getEdges().stream()).collect(Collectors.toList());

        assertThat(nodes).hasSize(3);
        assertThat(edges).hasSize(3);
    }

    @Test
    public void shouldCreateGraphWithTenNodes() {
        Graph g = new Graph(10, 40);

        Collection<Node> nodes = g.getNodes();
        Collection<Edge> edges = nodes.stream().flatMap(n -> n.getEdges().stream()).collect(Collectors.toList());

        assertThat(nodes).hasSize(10);
        assertThat(edges).hasSize(40);
    }
}