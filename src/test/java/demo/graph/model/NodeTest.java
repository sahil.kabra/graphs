package demo.graph.model;

import demo.graph.model.Edge;
import demo.graph.model.Node;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class NodeTest {

    @Test
    public void shouldAddEdgeForNode() {
        Node from = new Node("from");
        Node to = new Node("to");

        from.addEdge(to, 6);

        assertThat(from.getEdges())
                .extracting(Edge::getToNode)
                .extracting(Node::getName)
                .contains(to.getName());
    }

    @Test
    public void shouldReturnTrueIfDirectlyConnected() {
        Node from = new Node("from");
        Node target = new Node("target");

        from.addEdge(target, 6);

        assertThat(from.isDirectlyConnectedTo(target)).isTrue();
    }

    @Test
    public void shouldReturnFalseIfNotDirectlyConnected() {
        Node from = new Node("from");
        Node middle = new Node("middle");
        Node target = new Node("target");

        from.addEdge(middle, 6);
        middle.addEdge(target, 6);

        assertThat(from.isDirectlyConnectedTo(target)).isFalse();
    }

    @Test
    public void shouldReturnFalseIfNotConnected() {
        Node from = new Node("from");
        Node middle = new Node("middle");
        Node target = new Node("target");

        from.addEdge(middle, 6);

        assertThat(from.isDirectlyConnectedTo(target)).isFalse();
    }
}