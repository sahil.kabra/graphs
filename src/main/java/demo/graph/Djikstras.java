package demo.graph;

import com.google.common.collect.Lists;
import demo.graph.model.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Djikstras {
    public static Path findShortestPath(Graph g, String source, String target) {
        return findShortestPath(g, g.getNode(source), g.getNode(target));
    }

    public static Path findShortestPath(Graph g, Node source, Node target) {
        return calculateShortestDistances(g, source, target);
    }

    public static Path calculateShortestDistances(Graph g, Node source, Node target) {
        List<Node> nodes = new ArrayList<>(g.getNodes());

        List<Distance> distances = initDistances(nodes);
        Distance sourceDistance = findDistanceFor(distances, source);
        sourceDistance.setDistanceFromSource(0);

        List<Node> unvisitedNodes = new ArrayList<>();
        unvisitedNodes.add(source);

        populateDistances(distances, unvisitedNodes, new ArrayList<>());

        Distance targetDistance = findDistanceFor(distances, target);
        int weight = targetDistance.getDistanceFromSource();

        return new Path(
                getPathToSource(source, distances, targetDistance, new ArrayList<>()),
                weight == Integer.MAX_VALUE ? 0 : weight
        );
    }

    private static List<Node> getPathToSource(Node source, List<Distance> distances, Distance currentDistance, List<Node> path) {
        if (currentDistance.getNode() == source) {
            path.add(source);
            return Lists.reverse(path);
        }

        if (currentDistance.getEdge() == null) {
            return Collections.emptyList();
        }

        path.add(currentDistance.getNode());

        return getPathToSource(source, distances, findDistanceFor(distances, currentDistance.getEdge().getFromNode()), path);
    }

    private static void populateDistances(List<Distance> distances, List<Node> unvisitedNodes, List<Node> visitedNodes) {
        if (unvisitedNodes.isEmpty()) return;

        Node currentNode = unvisitedNodes.get(0);
        Distance currentNodeDistance = findDistanceFor(distances, currentNode);
        List<Edge> neighbours = currentNode.getEdges().stream().filter(e -> !visitedNodes.contains(e.getToNode())).collect(Collectors.toList());

        for (Edge e : neighbours) {
            Node neighbourNode = e.getToNode();
            Distance neighbourNodeDistance = findDistanceFor(distances, neighbourNode);

            if (currentNodeDistance.getDistanceFromSource() + e.getWeight() < neighbourNodeDistance.getDistanceFromSource()) {
                neighbourNodeDistance.setDistanceFromSource(currentNodeDistance.getDistanceFromSource() + e.getWeight());
                neighbourNodeDistance.setEdge(e);
            }
        }

        unvisitedNodes.remove(currentNode);
        visitedNodes.add(currentNode);

        unvisitedNodes.addAll(neighbours.stream()
                .map(Edge::getToNode)
                .filter(n -> !unvisitedNodes.contains(n))
                .sorted(Comparator.comparingInt(a -> findDistanceFor(distances, a).getDistanceFromSource()))
                .collect(Collectors.toList()));

        populateDistances(distances, unvisitedNodes, visitedNodes);
    }

    private static List<Distance> initDistances(List<Node> nodes) {
        List<Distance> distances = new ArrayList<>(nodes.size());

        for (Node n : nodes) {
            distances.add(new Distance(n));
        }
        return distances;
    }

    private static Distance findDistanceFor(List<Distance> distances, Node n) {
        return distances.stream().filter(d -> d.getNode() == n).findFirst().get();
    }
}
