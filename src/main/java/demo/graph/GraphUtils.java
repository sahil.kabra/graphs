package demo.graph;

import demo.graph.model.Graph;
import demo.graph.model.Node;
import demo.graph.model.Path;

import java.util.Comparator;

public class GraphUtils {

    public static Path shortestPath(Graph g, Node source, Node target) {
        return Djikstras.findShortestPath(g, source, target);
    }

    public static int eccentricity(Graph g, String source) {
        return eccentricity(g, g.getNode(source));
    }

    public static int eccentricity(Graph g, Node source) {
        return g.getNodes().stream()
                .map(n -> shortestPath(g, source, n))
                .max(Comparator.comparingInt(Path::getTotalWeight))
                .map(Path::getTotalWeight)
                .get();

    }

    public static int diameter(Graph g) {
        return g.getNodes().stream()
                .map(n -> eccentricity(g, n))
                .max(Comparator.naturalOrder())
                .get();
    }

    public static int radius(Graph g) {
        return g.getNodes().stream()
                .map(n -> eccentricity(g, n))
                .min(Comparator.naturalOrder())
                .get();
    }
}
