package demo.graph.model;

import lombok.Getter;

import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Getter
public class Node implements Comparable<Node> {
    private final String name;
    private final Set<Edge> edges;

    public Node(String name) {
        this.name = name;
        edges = new TreeSet<>();
    }

    public void addEdge(Node toNode, int weight) {
        edges.add(new Edge(this, toNode, weight));
    }

    public boolean isDirectlyConnectedTo(Node n) {
        return edges.stream().anyMatch(edge -> edge.getToNode().equals(n));
    }

    public String getRepr() {
        return this.edges.size() > 0
                ? edges.stream().map(Edge::toString).collect(Collectors.joining())
                : "";
    }

    @Override
    public String toString() {
        return String.format("Node(%s) -> Edges [%s]",
                name,
                edges.stream()
                        .map(e -> String.format("%s (w: %d)", e.getToNode().getName(), e.getWeight()))
                        .collect(Collectors.joining(", "))
        );
    }

    @Override
    public int compareTo(Node to) {
        return name.compareTo(to.name);
    }
}
