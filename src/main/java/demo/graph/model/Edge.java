package demo.graph.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import static java.lang.String.format;

@Getter
@AllArgsConstructor
public class Edge implements Comparable<Edge> {
    private final Node fromNode;
    private final Node toNode;
    private final int weight;

    public String toString() {
        return format("%s-> %s (%d)", fromNode.getName(), toNode.getName(), weight);
    }

    @Override
    public int compareTo(Edge to) {
        return toNode.compareTo(to.getToNode());
    }
}
