package demo.graph.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Path {
    private List<Node> nodes;
    private int totalWeight;
}
