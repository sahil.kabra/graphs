package demo.graph.model;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Getter
@Slf4j
public class Graph {
    private final Map<String, Node> nodes;
    private static final int MAX_WEIGHT = 10;

    public Graph(List<String> nodeNames) {
        nodes = new HashMap<>(nodeNames.size());

        for (String name: nodeNames) {
            nodes.put(name, new Node(name));
        }
    }

    public Graph(int numNodes, int numEdges) {
        nodes = initNodes(numNodes);

        addEdges(numEdges);
    }

    public boolean addEdge(String fromNodeName, String toNodeName, int weight) {
        assertNodeExists(fromNodeName);
        assertNodeExists(toNodeName);

        return addEdge(getNode(fromNodeName), getNode(toNodeName), weight);
    }

    public boolean addEdge(Node fromNode, Node toNode, int weight) {
        if (!fromNode.isDirectlyConnectedTo(toNode)) {
            fromNode.addEdge(toNode, weight);
            return true;
        } else {
            log.debug("{} and {} are already connected", fromNode.getName(), toNode.getName());
            return false;
        }
    }

    public Node getNode(String nodeName) {
        assertNodeExists(nodeName);
        return nodes.get(nodeName);
    }

    public Node getRandomNode(List<Node> nodes, Random r) {
        return nodes.get(r.nextInt(nodes.size()));
    }

    public String repr(String startNode) {
        assertNodeExists(startNode);

        return nodes.get(startNode).getRepr();
    }

    public ImmutableCollection<Node> getNodes() {
        ImmutableList.Builder<Node> b = ImmutableList.builder();
        return b.addAll(nodes.values()).build();
    }

    @Override
    public String toString() {
        return nodes.values().stream().map(Node::toString).collect(Collectors.joining("\n"));
    }

    private Map<String, Node> initNodes(int numNodes) {
        Map<String, Node> m = new HashMap<>(numNodes);
        for (int i = 0; i < numNodes; i++) {
            String nodeName = String.valueOf(i);
            m.put(nodeName, new Node(nodeName));
        }
        return m;
    }

    private void addEdges(int totalEdges) {
        List<Node> allNodes = new ArrayList<>(nodes.values());
        Node startNode = allNodes.parallelStream().findAny().get();

        allNodes.remove(startNode);

        Random r = new Random();
        // connect all nodes to each other starting with a random node
        connectNode(startNode, allNodes, r);

        // add the remaining edges
        int remainingEdges = totalEdges - (nodes.size() - 1);

        addAdditionalEdges(remainingEdges, r);
    }

    private void addAdditionalEdges(int number, Random r) {
        if (number <= 0) return;

        List<Node> allNodes = new ArrayList<>(nodes.values());
        Node from = getNodeWithNoEdge(allNodes).orElseGet(() -> getRandomNode(allNodes, r));

        allNodes.remove(from);

        Node to = getRandomNode(allNodes, r);

        addAdditionalEdges(addEdge(from, to, getRandomWeight(r)) ? number - 1 : number, r);
    }

    private Optional<Node> getNodeWithNoEdge(List<Node> nodes) {
        return nodes.stream().filter(n -> n.getEdges().isEmpty()).findFirst();
    }

    private void connectNode(Node node, Collection<Node> toNodes, Random r) {
        if (toNodes.isEmpty()) return;

        List<Node> allNodes = new ArrayList<>(toNodes);

        Node targetNode;

        if (allNodes.size() == 1) {
            targetNode = allNodes.stream().findFirst().get();
        } else {
            targetNode = getRandomNode(allNodes, r);
        }

        addEdge(node, targetNode, getRandomWeight(r));

        allNodes.remove(targetNode);

        connectNode(targetNode, allNodes, r);
    }

    private void assertNodeExists(String nodeName) {
        if (!nodes.containsKey(nodeName)) {
            throw new IllegalArgumentException(format("node %s not found", nodeName));
        }
    }

    private int getRandomWeight(Random r) {
        int w = 0;
        while (w == 0) {
            w = r.nextInt(MAX_WEIGHT);
        }
        return w;
    }
}

