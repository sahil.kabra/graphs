package demo.graph.model;

import lombok.Data;

@Data
public class Distance {
    private Node node;
    private Edge edge;
    private int distanceFromSource;

    public Distance(Node node) {
        this.node = node;
        this.distanceFromSource = Integer.MAX_VALUE;
    }
}