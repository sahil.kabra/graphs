package demo.graph;

import demo.graph.model.Graph;
import demo.graph.model.Node;
import demo.graph.model.Path;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class App {

    public static void main(String[] args) {
        if (args.length < 2) {
            throw new IllegalArgumentException("Please enter number of nodes and sparseness");
        }

        int numNodes = Integer.parseInt(args[0]);
        int numEdges = Integer.parseInt(args[1]);

        System.out.printf("Creating a graph with %d nodes and %d edges\n", numNodes, numEdges);
        System.out.println();

        Graph g = new Graph(numNodes, numEdges);
        Random r = new Random();
        List<Node> nodes = new ArrayList<>(g.getNodes());
        Node startNode = g.getRandomNode(nodes, r);

        nodes.remove(startNode);

        Node targetNode = g.getRandomNode(nodes, r);

        int radius = GraphUtils.radius(g);
        int diameter = GraphUtils.diameter(g);
        int eccentricity = GraphUtils.eccentricity(g, startNode);
        Path shortestPath = GraphUtils.shortestPath(g, startNode, targetNode);

        System.out.println();
        System.out.println("Graph created");
        System.out.println();
        System.out.println(g.toString());
        System.out.println();
        System.out.printf("Shortest path from %s to %s: %s (w: %d)\n",
                startNode.getName(),
                targetNode.getName(),
                shortestPath.getNodes().stream().map(Node::getName).collect(Collectors.joining(" -> ")),
                shortestPath.getTotalWeight()
        );
        System.out.printf("Eccentricity (%s): %d\n", startNode.getName(), eccentricity);
        System.out.printf("Diameter:  %d\n", diameter);
        System.out.printf("Radius:  %d\n", radius);
    }
}
